from django import forms
from django.utils.translation import ugettext_lazy as _
import os.path


class EjabberdImporterForm(forms.ModelForm):
    NOT_ABSOLUTE_PATH_ERROR = _("This storage path is not an absolute path.")
    NOT_DIRECTORY_ERROR = _("This storage path is not a directory.")

    def clean_storage_path(self):
        storage_path = self.cleaned_data['storage_path']

        if not os.path.isabs(storage_path):
            raise forms.ValidationError(self.NOT_ABSOLUTE_PATH_ERROR)

        if not os.path.isdir(storage_path):
            raise forms.ValidationError(self.NOT_DIRECTORY_ERROR)

        return storage_path
