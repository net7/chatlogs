from django.contrib import admin
from .forms import EjabberdImporterForm
from .models import EjabberdImporter


@admin.register(EjabberdImporter)
class EjabberdImporterAdmin(admin.ModelAdmin):
    form = EjabberdImporterForm
