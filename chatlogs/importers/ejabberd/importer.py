"""
Importer for ejabberd MUC logs.
"""

from django.db import transaction
from django.utils import timezone
from datetime import datetime, date, time
import heapq
import logging
import os
import re

from .models import EjabberdImporter

logger = logging.getLogger(__name__)


class Importer(object):
    """
    Main class for the ejabberd importer
    """

    DB_CLASS = EjabberdImporter
    LINE_RE = r"^(?:\[(\d\d):(\d\d):(\d\d)\] (?:<(.+?)> )?)?(.*)\n$"
    LINE_RE = re.compile(LINE_RE)

    def __init__(self, instance, fs_watcher):
        self._instance = instance
        self._chatroom = instance.target
        self._fs_watcher = fs_watcher
        self._cur_file = None
        self._buffer = None
        self._date_queue = []
        self._timezone = timezone.get_current_timezone()

        logger.info("Initializing ejabberd importer for path %s",
                    instance.storage_path)

        fs_watcher.add_dir(self, instance.storage_path)

        for item in os.listdir(self._instance.storage_path):
            self._add_to_queue(item)

        # Open the current log file, if any
        self._open_cur_file()

        if self._cur_file is not None:
            self._cur_file.seek(self._instance.cur_pos)

    def __repr__(self):
        return "<ejabberd.Importer for %r>" % self._instance.storage_path

    @transaction.atomic
    def process(self):
        """
        Processes the ejabberd logs.
        Called by the scheduler when either process(), file_added(),
        file_written() returned True on the previous scheduler call.
        Returns False if there is no more processing to do.
        """

        logger.debug("Processing for %s", self._instance.storage_path)

        if self._cur_file:
            if self._process_cur_file():
                # The current file is still in process
                return True

            if not self._date_queue:
                logger.debug("Done processing %r for now, waiting for data",
                             self._instance.cur_date)
                self._instance.save()
                return False

            # Prepare to move to the next file
            logger.debug("Closing log file for %s", self._instance.cur_date)
            self._fs_watcher.remove_file(self, self._cur_file)
            self._cur_file.close()
            self._cur_file = None

        if not self._date_queue:
            logger.debug("Nothing to do, sleeping")
            return False

        next_date = heapq.heappop(self._date_queue)
        logger.debug("Moving to log file for %s", next_date)

        self._instance.cur_date = next_date
        self._instance.cur_pos = 0
        self._instance.cur_time = None
        self._instance.cur_author = None
        self._instance.save()

        # Open the new log file
        self._open_cur_file()

        self._instance.save()

        return True

    def file_added(self, file_name):
        """
        Called by the scheduler when a new file is added to the watched
        directory.
        Returns True iff the file needs processing so the scheduler will call
        process().
        """

        return self._add_to_queue(file_name)

    def file_written(self, _fd):
        """
        Called by the scheduler when the current file gets written to.
        Returns True so the scheduler calls process() next.
        """

        return True

    def cleanup(self):
        """
        Importer clean-up before shutdown.
        """

        if self._cur_file is not None:
            self._fs_watcher.remove_file(self, self._cur_file)
            self._fs_watcher.remove_dir(self, self._instance.storage_path)
            self._cur_file.close()
            self._cur_file = None

    def _open_cur_file(self):
        """
        Opens the file for the current date, if it exists.
        """

        if not self._instance.cur_date:
            return

        file_name = self._instance.cur_date.strftime("%Y-%m-%d.txt")
        file_path = os.path.join(self._instance.storage_path, file_name)

        try:
            # Open the file with line buffering
            self._cur_file = open(file_path, 'r', buffering=1)
        except FileNotFoundError:
            logger.warning("File %s vanished, continuing anyway", file_name)
            self._cur_file = None

        if self._cur_file:
            self._fs_watcher.add_file(self, self._cur_file)

    def _process_cur_file(self):
        """
        Processes a number of lines from the currently opened file.
        Returns True iff there is still processing to do for this file.
        """

        for _ in range(100):
            if not self._process_next_line():
                self._instance.target.save()
                self._instance.save()
                return False

        return True

    def _process_next_line(self):
        """
        Processes the next line from the currently opened file.
        Returns True if the next line was read, False on EOF.
        """

        read_line = self._cur_file.readline()
        if not read_line:  # EOF
            return False

        if self._buffer is not None:
            read_line = self._buffer + read_line
            self._buffer = None

        try:
            groups = self.LINE_RE.match(read_line).groups()
            hour, min, sec, author, message = groups
        except AttributeError:
            # Incomplete line at EOF
            self._buffer = read_line
            return False

        if hour is None:
            msg_time = None
        else:
            try:
                msg_time = time(int(hour), int(min), int(sec))
            except ValueError:
                msg_time = None
                author = None
                message = read_line[:-1]

        self._instance.cur_pos += len(read_line)

        if msg_time is None:
            if self._instance.cur_time is None:
                logger.warn("Received continuation message “%s” at start "
                            "of file", message)
                msg_time = time(0, 0, 0)
            else:
                msg_time = self._instance.cur_time

            author = self._instance.cur_author
        else:
            self._instance.cur_time = msg_time
            self._instance.cur_author = author

        msg_date = datetime.combine(self._instance.cur_date, msg_time)
        msg_date = timezone.make_aware(msg_date, self._timezone)

        if author is not None:
            self._chatroom.add_message(author, msg_date, message)

        self._instance.target.last_message_time = msg_date

        return True

    def _add_to_queue(self, file_name):
        """
        Adds the specified file name to the file queue (as a date), at its
        expected position, only if:
         - it’s a log file
         - its date is more recent than the current date
         - it is not already in the queue.
        Returns True iff the file was added.
        """

        file_path = os.path.join(self._instance.storage_path, file_name)
        if not os.path.isfile(file_path):
            return False

        file_date = self._date_from_name(file_name)
        if file_date is None:
            return False

        if (self._instance.cur_date is not None and
                file_date <= self._instance.cur_date):
            return False

        if file_date in self._date_queue:
            return False

        logger.debug("Adding log file %s (date %s) to the log file queue",
                     file_name, file_date)
        heapq.heappush(self._date_queue, file_date)

        return True

    @staticmethod
    def _date_from_name(file_name):
        """
        Returns the log date if the file name provided corresponds to a log
        file name, or None.
        """

        match = re.match(r'^(\d+)-(\d+)-(\d+)\.txt', file_name)
        if match:
            year, month, day = match.groups()
            try:
                return date(int(year), int(month), int(day))
            except ValueError:
                pass

        return None
