from django.db import models
from django.utils.translation import ugettext_lazy as _

from chatlogs.models import ChatRoom, MAX_NAME_LENGTH


LOG_LANGUAGES = (
    ('fr', _("French")),
)

TARGET_HELP_TEXT = _("The room which will receive the ejabberd logs.")
STORAGE_PATH_HELP_TEXT = _("Filesystem path to the room logs directory")
LANGUAGE_HELP_TEXT = _("Language used by the ejabberd logger. Used to detect "
                       "joins, parts and topic changes.")


class EjabberdImporter(models.Model):
    target = models.ForeignKey(ChatRoom, verbose_name=_("Target room"),
                               help_text=TARGET_HELP_TEXT)

    storage_path = models.CharField(max_length=512,
                                    verbose_name=_("Storage path"),
                                    help_text=STORAGE_PATH_HELP_TEXT)

    log_language = models.CharField(max_length=2,
                                    verbose_name=_("Log language"),
                                    choices=LOG_LANGUAGES,
                                    help_text=LANGUAGE_HELP_TEXT)

    cur_date = models.DateField(editable=False, null=True, default=None)
    cur_pos = models.PositiveIntegerField(editable=False, default=0)
    cur_time = models.TimeField(editable=False, null=True, default=None)
    cur_author = models.CharField(max_length=MAX_NAME_LENGTH, editable=False,
                                  null=True, default=None)

    def __str__(self):
        result = _("Importer for {target} ({path})")
        return result.format(target=self.target, path=self.storage_path)

    class Meta:
        verbose_name = _("ejabberd importer")
        verbose_name_plural = _("ejabberd importers")
