from django.contrib import admin
from .models import ChatRoom


@admin.register(ChatRoom)
class ChatRoomAdmin(admin.ModelAdmin):
    prepopulated_fields = {
        'identifier': ('name',)
    }
