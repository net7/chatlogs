from colorsys import hsv_to_rgb
from enum import IntEnum
import binascii

from django.contrib.auth.models import Group, User
from django.core.urlresolvers import reverse
from django.db import models
from django.db.models import Q
from django.utils.translation import ugettext_lazy as _


MAX_NAME_LENGTH = 64


class TestModel(models.Model):
    date = models.DateTimeField()


class ChatRoomManager(models.Manager):
    def accessible_for(self, user):
        """
        Returns the chat rooms available for the specified user.
        """

        if user.has_perm('chatlogs.can_access_all_chat_rooms'):
            return self.all()

        if user.is_anonymous():
            return ChatRoom.objects.filter(is_public=True)

        return ChatRoom.objects.filter(Q(is_public=True) |
                                       Q(allowed_users=user) |
                                       Q(allowed_groups__user=user))


IDENTIFIER_HELP_TEXT = _("URL-safe chat room identifier")
IS_PUBLIC_HELP_TEXT = _("Public chatrooms will be accessible by all users, "
                        "even anonymous ones.")
ALLOWED_USERS_HELP_TEXT = _("For private chatrooms; indicates individual "
                            "users allowed to access it.")
ALLOWED_GROUPS_HELP_TEXT = _("For private chatrooms; indicates user groups "
                             "allowed to access it.")


class ChatRoom(models.Model):
    objects = ChatRoomManager()

    name = models.CharField(max_length=MAX_NAME_LENGTH, verbose_name=_("Name"))
    identifier = models.SlugField(unique=True, verbose_name=_("Identifier"),
                                  help_text=IDENTIFIER_HELP_TEXT)

    is_public = models.BooleanField(default=False,
                                    verbose_name=_("Public chatroom"),
                                    help_text=IS_PUBLIC_HELP_TEXT)

    allowed_users = models.ManyToManyField(User, blank=True, null=True,
                                           verbose_name=_('Allowed Users'),
                                           help_text=ALLOWED_USERS_HELP_TEXT)
    allowed_groups = models.ManyToManyField(Group, blank=True, null=True,
                                            verbose_name=_('Allowed Groups'),
                                            help_text=ALLOWED_GROUPS_HELP_TEXT)

    last_message_time = models.DateTimeField(null=True, editable=False)

    def add_message(self, nick, date, text):
        """
        Adds a normal message to the logs.
        """

        ChatLine.objects.create(line_type=ChatLineType.NORMAL,
                                author=self._get_author(nick), date=date,
                                text=text)

    def is_accessible_for(self, user):
        """
        Returns True iff the chat room is accessible for the specified user.
        """

        if user.has_perm('chatlogs.can_access_all_chat_rooms'):
            return True

        if user.is_anonymous():
            return self.is_public

        return ChatRoom.objects.filter(Q(id=self.id) & (Q(is_public=True) |
                                       Q(allowed_users=user) |
                                       Q(allowed_groups__user=user))).exists()

    def _get_author(self, nick):
        """
        Gets or creates an author entry for this chatroom, given its nick.
        """

        try:
            author_cache = self._author_cache
        except AttributeError:
            author_cache = self._author_cache = {}

        try:
            author = author_cache[nick]
        except KeyError:
            author, _ = Author.objects.get_or_create(nick=nick, room=self)
            author_cache[nick] = author

        return author

    def get_absolute_url(self):
        return reverse('chatlogs.views.chatroom_page',
                       args=[str(self.identifier)])

    def __str__(self):
        return self.name

    class Meta:
        permissions = (
            ("can_access_all_chat_rooms", _("Can access all rooms")),
        )
        verbose_name = _('Chat room')
        verbose_name_plural = _('Chat rooms')


class Author(models.Model):
    nick = models.CharField(max_length=MAX_NAME_LENGTH)
    color = models.CharField(editable=False, max_length=6)
    room = models.ForeignKey(ChatRoom)

    def save(self, *args, **kwargs):
        raw_value = binascii.crc32(self.nick.encode('utf-8')) & 0xFFFFFFFF
        hue = (raw_value & 0xFF) / 255.0
        saturation = ((raw_value >> 8) & 0xFF) / 512.0 + .5
        value = ((raw_value >> 16) & 0xFF) / 512.0 + .5

        red, green, blue = (round(i * 255.0)
                            for i in hsv_to_rgb(hue, saturation, value))

        self.color = "%02X%02X%02X" % (red, green, blue)

        super().save(*args, **kwargs)

    def __str__(self):
        return self.nick

    class Meta:
        unique_together = (('nick', 'room'),)


class ChatLineType(IntEnum):
    """
    Types of chat messages
    """

    NORMAL = 1          # Normal chat message
    NOTIFY_GENERIC = 2  # Notify (announcement, /me, …); no author
    NOTIFY_JOIN = 3     # Join notify; author = who joined
    NOTIFY_PART = 4     # Part notify; author = who joined, text = reason
    NOTIFY_TOPIC = 5    # Topic change notify; author=topic author, text=topic


class ChatLine(models.Model):
    line_type = models.PositiveSmallIntegerField()
    author = models.ForeignKey(Author, null=True)
    date = models.DateTimeField()
    text = models.TextField()

    types = ChatLineType  # To ease type selection from the template

    def __str__(self):
        if self.author:
            return "[%s] <%s> %s" % (self.date, self.author, self.text)

        return "[%s] * %s" % (self.date, self.text)
