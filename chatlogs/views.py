from datetime import datetime, time

from django.core.exceptions import PermissionDenied
from django.shortcuts import render, get_object_or_404, redirect
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _

from .models import ChatRoom, ChatLine


LOCAL_TIMEZONE = timezone.get_current_timezone()
CHATROOM_PERM_DENIED = _("You don’t have permission to view this chat room.")


def chatrooms_list(request):
    """
    Chatrooms list view
    """

    ctx = {
        'chatrooms': ChatRoom.objects.accessible_for(request.user),
    }

    return render(request, 'chatrooms_list.html', ctx)


def chatroom_page(request, identifier, date_string=None):
    """
    Chatroom log page
    """

    chatroom = get_object_or_404(ChatRoom, identifier=identifier)

    if not chatroom.is_accessible_for(request.user):
        raise PermissionDenied(CHATROOM_PERM_DENIED)

    if chatroom.last_message_time is None:
        # No message currently logged

        if date_string is not None:
            # Redirect to the chatroom main page
            return redirect(chatroom_page, identifier)

        logs = None
        req_date = None

    else:
        last_message_date = chatroom.last_message_time.date()

        try:
            req_date = datetime.strptime(date_string, '%Y-%m-%d').date()
        except (TypeError, ValueError):
            req_date = None

        if req_date is None or req_date > last_message_date:
            return redirect(chatroom_page, identifier,
                            last_message_date.strftime('%Y-%m-%d'))

        first_date = timezone.make_aware(datetime.combine(req_date, time.min),
                                         LOCAL_TIMEZONE)
        last_date = timezone.make_aware(datetime.combine(req_date, time.max),
                                        LOCAL_TIMEZONE)

        logs = ChatLine.objects.filter(author__room=chatroom,
                                       date__range=(first_date, last_date))
        logs = logs.select_related('author').order_by('id')

    ctx = {
        'chatroom': chatroom,
        'req_date': req_date,
        'logs': logs,
    }

    return render(request, 'chatroom_page.html', ctx)
