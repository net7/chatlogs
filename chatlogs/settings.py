import sys

try:
    from . import local_settings
except ImportError:
    sys.exit("Error: local_settings.py not found in the chatlogs directory.")

import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))

SECRET_KEY = local_settings.SECRET_KEY
DEBUG = local_settings.DEBUG
TEMPLATE_DEBUG = local_settings.DEBUG
ALLOWED_HOSTS = local_settings.ALLOWED_HOSTS
ADMINS = local_settings.ADMINS

ENABLED_IMPORTERS = local_settings.ENABLED_IMPORTERS

LOGIN_REDIRECT_URL = "/"

if DEBUG:
    EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

INSTALLED_APPS = (
    'chatlogs',  # Before the admin so we can override admin templates

    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    'registration',
) + tuple('chatlogs.importers.' + i for i in ENABLED_IMPORTERS)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'chatlogs.urls'

WSGI_APPLICATION = 'chatlogs.wsgi.application'

DATABASES = local_settings.DATABASES

LANGUAGE_CODE = local_settings.LANGUAGE_CODE

TIME_ZONE = local_settings.TIME_ZONE

USE_I18N = True
USE_L10N = True
USE_TZ = True

STATIC_URL = local_settings.STATIC_URL

ACCOUNT_ACTIVATION_DAYS = 7
REGISTRATION_AUTO_LOGIN = True

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse',
        },
        'require_debug_true': {
            '()': 'django.utils.log.RequireDebugTrue',
        },
    },
    'handlers': {
        'console_dbg_true': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'filters': ['require_debug_true'],
        },
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        },
        'syslog_dbg_false': {
            'level': 'INFO',
            'class': 'chatlogs.util.syslog.LocalSysLogHandler',
            'filters': ['require_debug_false'],
        },
    },
    'loggers': {
        'chatlogs.importers': {
            'handlers': [
                'console_dbg_true',
                'mail_admins',
                'syslog_dbg_false'
            ],
            'propagate': True,
            'level': 'DEBUG',
        },
    }
}


if local_settings.DB_DEBUG:
    LOGGING['loggers']['django.db'] = {
        'handlers': [],
        'level': 'DEBUG',
    }

del local_settings
