"""
ChatLogs installation-specific settings
"""

# #mark Django site settings — see Django docs for details

# Secret key (random string of text)
SECRET_KEY = ""

# Debug mode (do not use in production)
# Emails sent in debug mode will be written to the standard output
DEBUG = False

# Debug database requests to the console
DB_DEBUG = False

# List of allowed hosts
ALLOWED_HOSTS = []

# Administrator email addresses -- notified when errors occur on the website
ADMINS = (
    # ("Some admin name", "admin@example.org"), ...
)

# Database settings
# (https://docs.djangoproject.com/en/1.7/ref/settings/#databases)
DATABASES = {
    'default': {
        # Insert your database configuration here…
    }
}

# Internationalization (https://docs.djangoproject.com/en/1.7/topics/i18n/)
LANGUAGE_CODE = 'en-us'
TIME_ZONE = 'UTC'

# Static files load URL
# (https://docs.djangoproject.com/en/1.7/howto/static-files/)
STATIC_URL = '/static/'

# #mark ChatLogs settings

# Uncomment the import modules that you wish to use. You will be able to
# configure them in the admin panel.
ENABLED_IMPORTERS = (
    # 'ejabberd',
)
