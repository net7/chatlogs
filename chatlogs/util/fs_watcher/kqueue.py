"""
Filesystem watcher -- kqueue implementation
"""

from .base import FSWatcherBase
import os
import select
import sys


__all__ = ['FSWatcherKQueue']


class FSWatcherKQueue(FSWatcherBase):
    """
    kqueue-based filesystem watcher implementation; works on BSD-derived
    systems.
    """

    def __init__(self):
        self._watched_files = {}
        self._watched_dir_tuples = {}
        self._watched_dir_fds = {}

        self._queue = select.kqueue()
        try:
            self._open_flags = os.O_EVTONLY
        except AttributeError:
            if sys.platform == 'darwin':
                # O_EVTONLY value from fcntl.h
                self._open_flags = 0x8000  # pragma: no cover
            else:
                self._open_flags = os.O_RDONLY  # pragma: no cover

    def add_file(self, delegate, fileobj):
        """
        Adds a file object (for a normal file) to the watch list.
        The delegate will be notified when data is appended to the file.
        The file’s read position should be at the end before calling this
        function.
        """

        fd = fileobj.fileno()

        assert fd not in self._watched_files
        self._queue.control([select.kevent(fd, select.KQ_FILTER_READ,
                             select.KQ_EV_ADD | select.KQ_EV_CLEAR)], 0)
        self._watched_files[fd] = (delegate, fileobj)

    def remove_file(self, delegate, fileobj):
        """
        Remove a file object from the watch list.
        """

        fd = fileobj.fileno()

        assert fd in self._watched_files
        self._queue.control([select.kevent(fd, select.KQ_FILTER_READ,
                             select.KQ_EV_DELETE)], 0)
        del self._watched_files[fd]

    def add_dir(self, delegate, directory):
        """
        Adds a directory path to the watch list.
        The delegate will be notified when a new file is created in the
        directory.
        """

        assert (delegate, directory) not in self._watched_dir_tuples
        dirfd = os.open(directory, self._open_flags)
        self._watched_dir_tuples[(delegate, directory)] = dirfd

        event = select.kevent(dirfd, select.KQ_FILTER_VNODE,
                              select.KQ_EV_ADD | select.KQ_EV_CLEAR,
                              select.KQ_NOTE_WRITE)
        self._queue.control([event], 0)

        self._watched_dir_fds[dirfd] = (delegate, directory,
                                        frozenset(os.listdir(directory)))

    def remove_dir(self, delegate, directory):
        """
        Remove a directory path from the watch list.
        """

        dirfd = self._watched_dir_tuples.pop((delegate, directory))
        event = select.kevent(dirfd, select.KQ_FILTER_VNODE,
                              select.KQ_EV_DELETE, select.KQ_NOTE_WRITE)
        self._queue.control([event], 0)
        del self._watched_dir_fds[dirfd]
        os.close(dirfd)

    def wait(self, block=True):
        """
        Wait for changes on the file descriptors or the watched directories.
        Returns a (delegate, added items) list for watched directories, and a
        (delegate, changed fd) list for watched file descriptors
        If block if False, returns immediately with the intermediate changes.
        """

        notified_dirs = []
        notified_fds = []

        timeout = None if block else 0

        while not notified_dirs and not notified_fds:
            for event in self._queue.control(None, 10, timeout):
                fileno = event.ident
                file_tuple = self._watched_files.get(fileno)

                if file_tuple is not None:
                    notified_fds.append(file_tuple)
                    continue

                result = self._watched_dir_fds[fileno]
                delegate, directory, previous_list = result

                content = frozenset(os.listdir(directory))
                added_items = content - previous_list
                if added_items:
                    notified_dirs.append((delegate, added_items))

                self._watched_dir_fds[fileno] = delegate, directory, content

            if not block:
                break

        return notified_dirs, notified_fds

    def close(self):
        """
        Frees any resources allocated by the filesystem watcher
        """

        if self._watched_files is not None:
            for dirfd in self._watched_dir_fds.keys():
                os.close(dirfd)

            self._queue.close()

            self._watched_files = None
            self._watched_dir_tuples = None
            self._watched_dir_fds = None
            self._queue = None

    def __del__(self):
        self.close()
