"""
Filesystem watcher.
"""

from .generic import FSWatcherGeneric
import select

__all__ = ['FSWatcher']


FSWatcher = FSWatcherGeneric

if hasattr(select, 'kqueue'):
    from .kqueue import FSWatcherKQueue  # pragma: no cover
    FSWatcher = FSWatcherKQueue  # pragma: no cover

try:
    import pyinotify
    assert pyinotify  # pragma: no cover -- For pyflakes
except ImportError:  # pragma: no cover
    pass
else:
    from .inotify import FSWatcherINotify  # pragma: no cover
    FSWatcher = FSWatcherINotify  # pragma: no cover
