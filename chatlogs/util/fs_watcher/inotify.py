"""
Filesystem watcher -- inotify implementation
"""


from .base import FSWatcherBase
from pathlib import Path
import logging
import pyinotify


__all__ = ['FSWatcherINotify']


class FSWatcherINotify(FSWatcherBase, pyinotify.ProcessEvent):
    """
    inotify-based filesystem watcher implementation; works on Linux
    systems with the pyinotify module.
    """

    _inotify_logger = None

    def __init__(self):
        self._fd_delegate_wd = {}
        self._dirpath_delegate_wd = {}

        self._manager = pyinotify.WatchManager()
        self._notifier = pyinotify.Notifier(self._manager, self)

        self._notified_dirs = {}
        self._notified_fds = []

        if self._inotify_logger is None:
            # Mute pyinotify logger, as it produces bogus warnings
            # when a file is added to the watch list, modified and then
            # removed from the watch list
            self._inotify_logger = logging.getLogger("pyinotify")
            for handler in list(self._inotify_logger.handlers):
                self._inotify_logger.removeHandler(handler)
            self._inotify_logger.addHandler(logging.NullHandler())

    def add_file(self, delegate, fileobj):
        """
        Adds a file object (for a normal file) to the watch list.
        The delegate will be notified when data is appended to the file.
        The file’s read position should be at the end before calling this
        function.
        """

        fd = fileobj.fileno()

        assert fd not in self._fd_delegate_wd

        # We can’t pass file descriptors to inotify -- pass /proc/self/fd/<fd>
        # instead

        path = "/proc/self/fd/%d" % fd
        result = self._manager.add_watch(path, pyinotify.IN_MODIFY,
                                         quiet=False)

        # Store the delegate and the watch descriptor
        wdesc = result[path]
        self._fd_delegate_wd[fd] = (delegate, fileobj, wdesc)

    def remove_file(self, delegate, fileobj):
        """
        Remove a file object from the watch list.
        """

        fd = fileobj.fileno()

        f_delegate, _, wdesc = self._fd_delegate_wd.pop(fd, (None, None, None))
        assert delegate is f_delegate, "File to remove not found"

        self._manager.del_watch(wdesc)

    def add_dir(self, delegate, directory):
        """
        Adds a directory path to the watch list.
        The delegate will be notified when a new file is created in the
        directory.
        """

        assert directory not in self._dirpath_delegate_wd

        result = self._manager.add_watch(directory, pyinotify.IN_CREATE,
                                         quiet=False)

        # Store the delegate and the watch descriptor
        wdesc = result[directory]
        self._dirpath_delegate_wd[directory] = (delegate, wdesc)

    def remove_dir(self, delegate, directory):
        """
        Remove a directory path from the watch list.
        """

        d_delegate, wdesc = self._dirpath_delegate_wd.get(directory,
                                                          (None, None))
        assert delegate is d_delegate, "Directory to remove not found"

        self._manager.del_watch(wdesc)

    def wait(self, block=True):
        """
        Wait for changes on the file descriptors or the watched directories.
        Returns a (delegate, added items) list for watched directories, and a
        (delegate, changed fd) list for watched file descriptors
        If block if False, returns immediately with the intermediate changes.
        """

        self._notified_dirs.clear()
        self._notified_fds.clear()

        timeout = None if block else 0

        self._notifier.process_events()
        while self._notifier.check_events(timeout=timeout):
            self._notifier.read_events()
            self._notifier.process_events()

            # Only block once per wait
            timeout = 0

        notified_dirs = [(delegate, frozenset(files))
                         for (delegate, files)
                         in self._notified_dirs.items()]

        return notified_dirs, self._notified_fds

    def close(self):
        """
        Frees any resources allocated by the filesystem watcher
        """

        if self._manager is not None:
            self._manager.close()
            self._manager = None

    def process_IN_MODIFY(self, event):
        """
        Called by inotify when a watched file is modified.
        """

        # Get FD from /proc/self/fd/<fd> path
        fd = int(Path(event.path).name)

        delegate, fileobj, _ = self._fd_delegate_wd[fd]
        self._notified_fds.append((delegate, fileobj))

    def process_IN_CREATE(self, event):
        """
        Called by inotify when a file is created in a watched directory.
        """

        path = event.path
        file_name = event.name

        delegate, _ = self._dirpath_delegate_wd[path]

        try:
            self._notified_dirs[delegate].add(file_name)
        except KeyError:
            self._notified_dirs[delegate] = {file_name}

    def __del__(self):
        self.close()
