"""
Filesystem watcher -- base class
"""

__all__ = ['FSWatcherBase']


class FSWatcherBase(object):
    """
    Common base for all filesystem watcher implementations
    """

    def add_file(self, delegate, fileobj):
        """
        Adds a file object (for a normal file) to the watch list.
        The delegate will be notified when data is appended to the file.
        The file’s read position should be at the end before calling this
        function.
        """

        raise NotImplementedError("Must be implemented in subclasses")

    def remove_file(self, delegate, fileobj):
        """
        Remove a file object from the watch list.
        """

        raise NotImplementedError("Must be implemented in subclasses")

    def add_dir(self, delegate, directory):
        """
        Adds a directory path to the watch list.
        The delegate will be notified when a new file is created in the
        directory.
        """

        raise NotImplementedError("Must be implemented in subclasses")

    def remove_dir(self, delegate, directory):
        """
        Remove a directory path from the watch list.
        """

        raise NotImplementedError("Must be implemented in subclasses")

    def wait(self, block=True):
        """
        Wait for changes on the file descriptors or the watched directories.
        Returns a (delegate, added items) list for watched directories, and a
        (delegate, changed fd) list for watched file descriptors
        If block if False, returns immediately with the intermediate changes.
        """

        raise NotImplementedError("Must be implemented in subclasses")

    def __enter__(self):
        """
        Enters the context manager.
        """

        return self

    def __exit__(self, exc_type, exc_value, traceback):
        """
        Exits the context manager.
        """

        self.close()

        return False

    def close(self):
        """
        Frees any resources allocated by the filesystem watcher
        """

        return
