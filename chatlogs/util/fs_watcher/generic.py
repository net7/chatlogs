"""
Filesystem watcher -- generic (portable but slow and polling) implementation
"""

from .base import FSWatcherBase
import os
import time


__all__ = ['FSWatcherGeneric']


class FSWatcherGeneric(FSWatcherBase):
    """
    Generic filesystem watcher implementation; polls for changes every 5
    seconds.
    """

    def __init__(self):
        self._watched_dirs = []
        self._watched_files = []
        self._dir_state = {}

    def add_file(self, delegate, fileobj):
        """
        Adds a file object (for a normal file) to the watch list.
        The delegate will be notified when data is appended to the file.
        The file’s read position should be at the end before calling this
        function.
        """

        self._watched_files.append((delegate, fileobj))

    def remove_file(self, delegate, fileobj):
        """
        Remove a file object from the watch list.
        """

        self._watched_files.remove((delegate, fileobj))

    def add_dir(self, delegate, directory):
        """
        Adds a directory path to the watch list.
        The delegate will be notified when a new file is created in the
        directory.
        """

        self._watched_dirs.append((delegate, directory))

        if directory not in self._dir_state:
            self._dir_state[directory] = frozenset(os.listdir(directory))

    def remove_dir(self, delegate, directory):
        """
        Remove a directory path from the watch list.
        """

        self._watched_dirs.remove((delegate, directory))

    def wait(self, block=True):
        """
        Wait for changes on the file descriptors or the watched directories.
        Returns a (delegate, added items) list for watched directories, and a
        (delegate, changed fd) list for watched file descriptors
        If block if False, returns immediately with the intermediate changes.
        """

        notified_dirs = []
        notified_fds = []

        while not notified_dirs and not notified_fds:
            for delegate, fileobj in self._watched_files:
                pos = fileobj.tell()
                data = fileobj.read(1)
                fileobj.seek(pos)
                if data:
                    notified_fds.append((delegate, fileobj))

            for delegate, directory in self._watched_dirs:
                content = frozenset(os.listdir(directory))
                added_items = content - self._dir_state[directory]
                if added_items:
                    notified_dirs.append((delegate, added_items))

                self._dir_state[directory] = content

            if not notified_dirs and not notified_fds:
                if not block:
                    return [], []
                time.sleep(5)

        return notified_dirs, notified_fds
