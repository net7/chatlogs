"""
syslog(3) handler for Python’s logging system
"""

from logging.handlers import SysLogHandler
import logging
import syslog


class LocalSysLogHandler(logging.Handler):
    """
    syslog handler class
    """

    def __init__(self):
        super().__init__()

        syslog.openlog("chatlogs", syslog.LOG_NDELAY, syslog.LOG_DAEMON)

    def emit(self, record):
        try:
            self._emit(record)
        except Exception:
            self.handleError(record)

    def _emit(self, record):
        msg = self.format(record)

        priority_name = SysLogHandler.mapPriority(SysLogHandler,
                                                  record.levelname)
        priority = SysLogHandler.priority_names[priority_name]

        for line in msg.split("\n"):
            syslog.syslog(priority, line)

    def close(self):
        syslog.closelog()
