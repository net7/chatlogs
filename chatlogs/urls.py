from django.conf.urls import include, url
from django.contrib import admin

urlpatterns = (
    url(r'^$', 'chatlogs.views.chatrooms_list'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^accounts/', include('registration.backends.default.urls')),
    url(r'^(?P<identifier>[a-zA-Z0-9_-]+)/$', 'chatlogs.views.chatroom_page'),
    url(r'^(?P<identifier>[a-zA-Z0-9_-]+)/(?P<date_string>\d{4}-\d{2}-\d{2})$',
        'chatlogs.views.chatroom_page'),
)
