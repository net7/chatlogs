from django.conf import settings
from django.core.management.base import NoArgsCommand
from importlib import import_module
import os
import logging

from chatlogs.util.fs_watcher import FSWatcher


LOGGER = logging.getLogger('chatlogs.importers')


class Command(NoArgsCommand):
    """
    Definition of the run_importers command
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._importers = []

    def handle_noargs(self, **options):
        """
        Handles the run_importer command
        """

        try:
            with FSWatcher() as watcher:
                try:
                    needs_processing = self._setup(watcher)
                    self._run(watcher, needs_processing)
                finally:
                    self._cleanup()
        except KeyboardInterrupt:
            pass
        except Exception:
            LOGGER.exception("Exception during importer processing")
            os._exit(1)

    def _setup(self, watcher):
        """
        Initializes importer instances.
        Return a set of importer instances.
        """

        for importer_name in settings.ENABLED_IMPORTERS:
            module_name = "chatlogs.importers.%s.importer" % importer_name
            module = import_module(module_name)
            importer_class = module.Importer
            db_class = importer_class.DB_CLASS

            for instance in db_class.objects.all():
                importer = importer_class(instance, watcher)
                self._importers.append(importer)

        return set(self._importers)

    def _run(self, watcher, needs_processing):
        """
        Run the importers according to their scheduling needs.
        """

        while True:
            block = not needs_processing
            notified_dirs, notified_fds = watcher.wait(block=block)

            for importer, added_items in notified_dirs:
                for added_item in added_items:
                    if importer.file_added(added_item):
                        needs_processing.add(importer)

            for importer, notified_fd in notified_fds:
                if importer.file_written(notified_fd):
                    needs_processing.add(importer)

            to_process = list(needs_processing)
            needs_processing.clear()
            for importer in to_process:
                if importer.process():
                    needs_processing.add(importer)

    def _cleanup(self):
        """
        Cleans the importers up.
        """

        for importer in self._importers:
            try:
                importer.cleanup()
            except Exception:
                LOGGER.exception("Exception during cleanup of importer %r:",
                                 importer)
