"""
Chatroom permissions test.
"""

from django.test import Client, TestCase


class ChatroomAuthTestCase(TestCase):
    fixtures = ['users_and_rooms']
    rooms = [
        'public-room',
        'group-1-room',
        'group-2-room',
        'user-room',
        'admin-only-room',
    ]

    def test_unauth(self):
        client = Client()
        self.check_access_only_to(client, {'public-room'})

    def test_admin(self):
        client = Client()
        result = client.login(username='test_admin', password='test_admin')
        self.assertTrue(result)
        self.check_access_only_to(client, {
            'public-room',
            'group-1-room',
            'group-2-room',
            'user-room',
            'admin-only-room',
        })

    def test_access_perm(self):
        client = Client()
        result = client.login(username='test_access_perm',
                              password='test_access_perm')
        self.assertTrue(result)
        self.check_access_only_to(client, {
            'public-room',
            'group-1-room',
            'group-2-room',
            'user-room',
            'admin-only-room',
        })

    def test_in_group1(self):
        client = Client()
        result = client.login(username='test_in_group1',
                              password='test_in_group1')
        self.assertTrue(result)
        self.check_access_only_to(client, {
            'public-room',
            'group-1-room',
        })

    def test_in_group2(self):
        client = Client()
        result = client.login(username='test_in_group2',
                              password='test_in_group2')
        self.assertTrue(result)
        self.check_access_only_to(client, {
            'public-room',
            'group-2-room',
        })

    def test_no_groups_room_perm(self):
        client = Client()
        result = client.login(username='test_no_groups',
                              password='test_no_groups')
        self.assertTrue(result)
        self.check_access_only_to(client, {
            'public-room',
            'user-room',
        })

    def check_access_only_to(self, client, expected_identifiers):
        """
        Checks that the client has access only to the specified chat room
        identifiers
        """

        response = client.get("/")
        self.assertEquals(response.status_code, 200)
        chatrooms = response.context['chatrooms']
        identifiers = set(chatroom.identifier for chatroom in chatrooms)
        self.assertEquals(identifiers, expected_identifiers)

        for identifier in self.rooms:
            response = client.get("/%s/" % identifier)

            if identifier in expected_identifiers:
                self.assertEquals(response.status_code, 200)
            else:
                self.assertEquals(response.status_code, 403)
