"""
Syslog logging handler tests.
"""

from unittest.mock import patch, ANY, call, NonCallableMagicMock
from chatlogs.util.syslog import LocalSysLogHandler
import syslog
import logging
import unittest


@patch('syslog.syslog')
class SyslogHandlerTestCase(unittest.TestCase):
    @patch('syslog.openlog')
    def setUp(self, mock_openlog):
        self._handler = LocalSysLogHandler()
        mock_openlog.assert_called_with(ANY, ANY, ANY)

    def test_syslog_info(self, mock_syslog):
        logger = logging.getLogger('__test__')
        logger.addHandler(self._handler)
        logger.setLevel(logging.INFO)
        logger.info("This is\na test.é")
        mock_syslog.assert_has_calls([
            call(syslog.LOG_INFO, "This is"),
            call(syslog.LOG_INFO, "a test.é"),
        ])

    def test_syslog_debug(self, mock_syslog):
        logger = logging.getLogger('__test__')
        logger.addHandler(self._handler)
        logger.setLevel(logging.INFO)
        logger.debug("This is\na test.é")
        mock_syslog.assert_has_no_calls()

    @patch('sys.stderr', new_callable=NonCallableMagicMock)
    def test_syslog_exception(self, mock_sys_stderr, mock_syslog):
        mock_syslog.side_effect = Exception("BOOM")
        logger = logging.getLogger('__test__')
        logger.addHandler(self._handler)
        logger.setLevel(logging.INFO)
        logger.info("This is\na test.é")
        mock_syslog.side_effect = None
        mock_sys_stderr.assert_has_calls([
            call.write(ANY),
        ])

    @patch('syslog.closelog')
    def tearDown(self, mock_closelog):
        self._handler.close()
        mock_closelog.assert_called_with()
