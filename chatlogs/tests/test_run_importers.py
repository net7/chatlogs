"""
run_importers command test.
"""

from django.conf import settings
from unittest.mock import Mock, patch, call, sentinel
import imp
import sys
import unittest

from chatlogs.management.commands.run_importers import Command as RunImporters

IMPORTER_PATH = 'chatlogs.importers.fake_importer.importer'
MOCK_IMPORTER = Mock()


def fake_exit(status):
    """
    Replacement for os._exit, called by run_importers when an error occurs
    Re-raises the currently raised exception
    """

    raise


@patch('os._exit', fake_exit)
@patch('chatlogs.management.commands.run_importers.FSWatcher')
class RunImportersTestCase(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._saved_settings = None
        self._command = RunImporters()

    def setUp(self):
        self._saved_settings = settings.ENABLED_IMPORTERS
        settings.ENABLED_IMPORTERS = []

        assert IMPORTER_PATH not in sys.modules
        module = sys.modules[IMPORTER_PATH] = imp.new_module(IMPORTER_PATH)

        module.Importer = MOCK_IMPORTER

    def test_no_importers(self, mock_fs_watcher):
        """
        No importers; run_importers should call the FS watcher in blocking
        mode and then do nothing.
        """

        entered_fs_watcher = mock_fs_watcher().__enter__()

        entered_fs_watcher.wait.side_effect = KeyboardInterrupt

        self._command.handle()

        entered_fs_watcher.assert_has_calls([call.wait(block=True)])

    def test_one_importer(self, mock_fs_watcher):
        """
        One importer, with one instance; run_importers should initialize it,
        call the FS watcher in non-blocking mode, call the importer process
        function, and then call the FS watcher in blocking mode.
        """

        settings.ENABLED_IMPORTERS = ['fake_importer']

        db_class = MOCK_IMPORTER.DB_CLASS
        db_class.objects.all.side_effect = lambda: [sentinel.instance]
        entered_fs_watcher = mock_fs_watcher().__enter__()
        entered_fs_watcher.wait.side_effect = [
            ([], []),  # No files are ready
            KeyboardInterrupt,  # User interrupt
        ]

        # The importer has nothing to process
        MOCK_IMPORTER().process.side_effect = lambda: False
        entered_fs_watcher.reset_mock()
        MOCK_IMPORTER.reset_mock()

        self._command.handle()

        MOCK_IMPORTER.assert_has_calls([
            call.DB_CLASS.objects.all(),  # Asking for DB instances
            call(sentinel.instance, entered_fs_watcher),  # Importer creation
            call().process(),  # Importer first process
            call().cleanup(),  # Importer cleanup
        ])
        entered_fs_watcher.assert_has_calls([
            call.wait(block=False),
            call.wait(block=True)
        ])

    def test_one_importer_exc(self, mock_fs_watcher):
        """
        One importer, with one instance, which causes an exception in
        its process; the importer cleanup should be called (it will also cause
        and exception, which should _not_ be propagated), and then the
        exception should cause a call to os._exit(1)
        """

        settings.ENABLED_IMPORTERS = ['fake_importer']

        db_class = MOCK_IMPORTER.DB_CLASS
        db_class.objects.all.side_effect = lambda: [sentinel.instance]
        entered_fs_watcher = mock_fs_watcher().__enter__()
        entered_fs_watcher.wait.side_effect = [
            ([], []),  # No files are ready
            KeyboardInterrupt,  # User interrupt
        ]

        # The importer causes an exception in its process method...
        MOCK_IMPORTER().process.side_effect = Exception("ProcessExc")

        # and during the execution of its cleanup method
        MOCK_IMPORTER().cleanup.side_effect = Exception("CleanupExc")

        entered_fs_watcher.reset_mock()
        MOCK_IMPORTER.reset_mock()

        with self.assertRaisesRegexp(Exception, r"^ProcessExc$"):
            self._command.handle()

        MOCK_IMPORTER.assert_has_calls([
            call.DB_CLASS.objects.all(),  # Asking for DB instances
            call(sentinel.instance, entered_fs_watcher),  # Importer creation
            call().process(),  # Importer first process
            call().cleanup(),  # Importer cleanup
        ])
        entered_fs_watcher.assert_has_calls([call.wait(block=False)])

    def test_one_importer_notifications(self, mock_fs_watcher):
        """
        One importer, with one instance with one directory notification and
        two files notifications.
        The importer’s process() method should be called (=> False), then its
        file_added method (once => True), then its file_written method
        (twice => False and True), then process() again (=> True), and
        process() again (=>False). The FS watcher will then be called in
        blocking mode.
        """

        settings.ENABLED_IMPORTERS = ['fake_importer']

        db_class = MOCK_IMPORTER.DB_CLASS
        db_class.objects.all.side_effect = lambda: [sentinel.instance]
        importer_instance = MOCK_IMPORTER()
        entered_fs_watcher = mock_fs_watcher().__enter__()
        entered_fs_watcher.wait.side_effect = [
            # The importer hasn’t run yet -- there can’t be any files
            ([], []),
            (
                [(importer_instance, frozenset(['added_file']))],
                [
                    (importer_instance, sentinel.fd_1),
                    (importer_instance, sentinel.fd_2),
                ]
            ),  # One added file, two notified FDs
            ([], []),  # No files this time
            KeyboardInterrupt,  # User interrupt
        ]

        # Process return False, True, and False
        importer_instance.process.side_effect = [
            False,
            True,
            False,
        ]

        # file_added returns True
        importer_instance.file_added.side_effect = [
            True,
        ]

        # file_modified returns False and then True
        importer_instance.file_modified.side_effect = [
            False,
            True,
        ]

        entered_fs_watcher.reset_mock()
        MOCK_IMPORTER.reset_mock()

        self._command.handle()

        MOCK_IMPORTER.assert_has_calls([
            call.DB_CLASS.objects.all(),  # Asking for DB instances
            call(sentinel.instance, entered_fs_watcher),  # Importer creation
            call().process(),  # Importer first process
            call().file_added('added_file'),  # Added file notification
            call().file_written(sentinel.fd_1),  # Modified file notification
            call().file_written(sentinel.fd_2),  # for both files
            call().process(),  # Process again (returns True)
            call().process(),  # Final process (returns False)
            call().cleanup(),  # Importer cleanup
        ])
        entered_fs_watcher.assert_has_calls([
            call.wait(block=False),  # Initial call
            call.wait(block=True),   # The process has returned False
            call.wait(block=False),  # The process has returned True
            call.wait(block=True),   # The process has returned False
        ])

    def tearDown(self):
        settings.ENABLED_IMPORTERS = self._saved_settings
        del sys.modules[IMPORTER_PATH]
