"""
Filesystem watcher tests
"""

from chatlogs.util import fs_watcher
from pathlib import Path
import os
import select
import shutil
import tempfile
import threading
import unittest

try:
    import pyinotify
except ImportError:
    pyinotify = None


class TemporaryDirectoryPath(object):
    """
    Temporary directory path class.
    """

    def __init__(self):
        self._path = Path(tempfile.mkdtemp())

    def open_file(self, name, mode='a'):
        """
        Opens a file in the directory, and returns the file object.
        """

        return (self._path / name).open(mode=mode)

    def cleanup(self):
        """
        Removes the temporary directory
        """

        if self._path is not None:
            shutil.rmtree(str(self._path))
            self._path = None

    def __str__(self):
        return str(self._path)

    def __del__(self):
        self.cleanup()


class FSWatcherBaseTestCase(object):
    """
    Base test case class common to file watcher tests
    """

    watcher_class = None

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._temp_dir = None
        self._file_write = None
        self._orig_fds = None

    @staticmethod
    def list_opened_fds():
        """
        Get a list of opened file descriptors.
        """

        try:
            fds = os.listdir("/dev/fd/")
        except FileNotFoundError:
            try:
                fds = os.listdir("/proc/self/fd/")
            except FileNotFoundError:
                fds = []

        fd_set = set(int(fd) for fd in fds)

        # os.listdir allocates a FD when it runs; since we’re hopefully
        # not multithreaded at this point, os.open will return the same FD.
        used_fd = os.open(os.devnull, os.O_RDONLY)
        os.close(used_fd)
        fd_set.discard(used_fd)

        return fd_set

    def setUp(self):
        self._temp_dir = TemporaryDirectoryPath()
        self._orig_fds = self.list_opened_fds()

    def test_empty_nonblocking(self):
        """
        No watched directories; non-blocking mode.
        The wait should return immediately without any notifications.
        """

        outer = self

        class ThreadedFunc(threading.Thread):
            def __init__(self):
                super().__init__(name="test_empty_nonblocking", daemon=True)
                self.notified_dirs = None
                self.notified_fds = None
                self.start()

            def run(self):
                with outer.watcher_class() as watcher:
                    result = watcher.wait(block=False)
                    self.notified_dirs, self.notified_fds = result

        thread = ThreadedFunc()
        thread.join(.1)
        self.assertTrue(not thread.is_alive())
        self.assertEquals(thread.notified_dirs, [])
        self.assertEquals(thread.notified_fds, [])

    def test_non_modified_dir_nonblocking(self):
        """
        One watched directory, not modified; non-blocking mode
        The wait should return immediately without any notifications.
        """

        outer = self

        class ThreadedFunc(threading.Thread):
            def __init__(self):
                super().__init__(name="test_non_modified_dir_nonblocking",
                                 daemon=True)
                self.notified_dirs = None
                self.notified_fds = None
                self.start()

            def run(self):
                with outer.watcher_class() as watcher:
                    watcher.add_dir(self, str(outer._temp_dir))
                    result = watcher.wait(block=False)
                    self.notified_dirs, self.notified_fds = result

        thread = ThreadedFunc()
        thread.join(.1)
        self.assertTrue(not thread.is_alive())
        self.assertEquals(thread.notified_dirs, [])
        self.assertEquals(thread.notified_fds, [])

    def test_previous_mod_dir_nonblocking(self):
        """
        One watched directory, modified before the wait; non-blocking mode
        The wait should return immediately with a modified directory
        notification.
        """
        outer = self

        class ThreadedFunc(threading.Thread):
            def __init__(self):
                super().__init__(name="test_previous_mod_dir_nonblocking",
                                 daemon=True)
                self.notified_dirs = None
                self.notified_fds = None
                self.start()

            def run(self):
                with outer.watcher_class() as watcher:
                    watcher.add_dir(self, str(outer._temp_dir))
                    with outer._temp_dir.open_file("test_file") as f:
                        f.write("test")
                    result = watcher.wait(block=False)
                    self.notified_dirs, self.notified_fds = result

        thread = ThreadedFunc()
        thread.join(.1)
        self.assertTrue(not thread.is_alive())
        self.assertEquals(thread.notified_dirs,
                          [(thread, frozenset(["test_file"]))])
        self.assertEquals(thread.notified_fds, [])

    def test_previous_mod_dir_rmed_nonblocking(self):
        """
        One watched directory, modified before the wait, then removed;
        non-blocking mode
        The wait should return immediately without notifications.
        """
        outer = self

        class ThreadedFunc(threading.Thread):
            def __init__(self):
                super().__init__(name="test_previous_mod_dir_rmed_nonblocking",
                                 daemon=True)
                self.notified_dirs = None
                self.notified_fds = None
                self.start()

            def run(self):
                with outer.watcher_class() as watcher:
                    watcher.add_dir(self, str(outer._temp_dir))
                    with outer._temp_dir.open_file("test_file") as f:
                        f.write("test")
                    watcher.remove_dir(self, str(outer._temp_dir))
                    result = watcher.wait(block=False)
                    self.notified_dirs, self.notified_fds = result

        thread = ThreadedFunc()
        thread.join(.1)
        self.assertTrue(not thread.is_alive())
        self.assertEquals(thread.notified_dirs, [])
        self.assertEquals(thread.notified_fds, [])

    def test_previous_mod_dir_blocking(self):
        """
        One watched directory, modified before the wait; blocking mode
        The wait should return immediately with a modified directory
        notification.
        """
        outer = self

        class ThreadedFunc(threading.Thread):
            def __init__(self):
                super().__init__(name="test_previous_mod_dir_blocking",
                                 daemon=True)
                self.notified_dirs = None
                self.notified_fds = None
                self.start()

            def run(self):
                with outer.watcher_class() as watcher:
                    watcher.add_dir(self, str(outer._temp_dir))
                    with outer._temp_dir.open_file("test_file") as f:
                        f.write("test")
                    result = watcher.wait(block=True)
                    self.notified_dirs, self.notified_fds = result

        thread = ThreadedFunc()
        thread.join(.1)
        self.assertTrue(not thread.is_alive())
        self.assertEquals(thread.notified_dirs,
                          [(thread, frozenset(["test_file"]))])
        self.assertEquals(thread.notified_fds, [])

    def test_after_mod_dir_blocking(self):
        """
        One watched directory, modified after the wait; blocking mode
        The wait should return quickly enough with a modified directory
        notification.
        """
        outer = self
        started_evt = threading.Event()

        class ThreadedFunc(threading.Thread):
            def __init__(self):
                super().__init__(name="test_previous_mod_dir_blocking",
                                 daemon=True)
                self.notified_dirs = None
                self.notified_fds = None
                self.start()

            def run(self):
                with outer.watcher_class() as watcher:
                    watcher.add_dir(self, str(outer._temp_dir))
                    started_evt.set()
                    result = watcher.wait(block=True)
                    self.notified_dirs, self.notified_fds = result

        thread = ThreadedFunc()
        self.assertTrue(started_evt.wait(1))
        thread.join(1)  # Test if the thread doesn’t prematurely return
        self.assertTrue(thread.is_alive())
        with self._temp_dir.open_file("test_file") as f:
            f.write("test")
        thread.join(6)
        self.assertTrue(not thread.is_alive())
        self.assertEquals(thread.notified_dirs,
                          [(thread, frozenset(["test_file"]))])
        self.assertEquals(thread.notified_fds, [])

    def test_previous_mod_file_nonblocking(self):
        """
        One watched file, modified before the wait; non-blocking mode
        The wait should return immediately with a modified file notification.
        """
        outer = self

        self._file_write = self._temp_dir.open_file("test_file", mode='w')
        with self._file_write:
            class ThreadedFunc(threading.Thread):
                def __init__(self):
                    super().__init__(name="test_previous_mod_file_nonblocking",
                                     daemon=True)
                    self.notified_dirs = None
                    self.notified_fds = None
                    self.assigned_fd = None
                    self.start()

                def run(self):
                    with outer.watcher_class() as watcher:
                        fd = outer._temp_dir.open_file("test_file", mode='r')
                        self.assigned_fd = fd
                        watcher.add_file(self, self.assigned_fd)
                        outer._file_write.write("Test")
                        outer._file_write.flush()

                        results = watcher.wait(block=False)
                        self.notified_dirs, self.notified_fds = results

            thread = ThreadedFunc()
            thread.join(.1)
            self.assertTrue(not thread.is_alive())
            self.assertEquals(thread.notified_dirs, [])
            self.assertEquals(thread.notified_fds,
                              [(thread, thread.assigned_fd)])
            thread.assigned_fd.close()

    def test_previous_mod_file_removed_nonblocking(self):
        """
        One watched file, modified before the wait, then removed; non-blocking
        mode
        The wait should return immediately without any notifications.
        """
        outer = self

        self._file_write = self._temp_dir.open_file("test_file", mode='w')
        with self._file_write:
            class ThreadedFunc(threading.Thread):
                def __init__(self):
                    super().__init__(name="test_previous_mod_file_nonblocking",
                                     daemon=True)
                    self.notified_dirs = None
                    self.notified_fds = None
                    self.assigned_fd = None
                    self.start()

                def run(self):
                    with outer.watcher_class() as watcher:
                        fd = outer._temp_dir.open_file("test_file", mode='r')
                        self.assigned_fd = fd
                        watcher.add_file(self, self.assigned_fd)
                        outer._file_write.write("Test")
                        outer._file_write.flush()
                        watcher.remove_file(self, self.assigned_fd)

                        result = watcher.wait(block=False)
                        self.notified_dirs, self.notified_fds = result

            thread = ThreadedFunc()
            thread.join(.1)
            self.assertTrue(not thread.is_alive())
            self.assertEquals(thread.notified_dirs, [])
            self.assertEquals(thread.notified_fds, [])
            thread.assigned_fd.close()

    def test_after_mod_file_blocking(self):
        """
        One watched file, modified after the wait; blocking mode
        The wait should return quickly enough with a modified file
        notification.
        """
        outer = self
        started_evt = threading.Event()

        self._file_write = self._temp_dir.open_file("test_file", mode='w')
        with self._file_write:
            class ThreadedFunc(threading.Thread):
                def __init__(self):
                    super().__init__(name="test_after_mod_file_blocking",
                                     daemon=True)
                    self.notified_dirs = None
                    self.notified_fds = None
                    self.start()

                def run(self):
                    with outer.watcher_class() as watcher:
                        fd = outer._temp_dir.open_file("test_file", mode='r')
                        self.assigned_fd = fd
                        watcher.add_file(self, self.assigned_fd)
                        started_evt.set()
                        result = watcher.wait(block=True)
                        self.notified_dirs, self.notified_fds = result

            thread = ThreadedFunc()
            self.assertTrue(started_evt.wait(1))
            thread.join(1)  # Test if the thread doesn’t prematurely return
            self.assertTrue(thread.is_alive())
            self._file_write.write("test")
            self._file_write.flush()
            thread.join(6)
            self.assertTrue(not thread.is_alive())
            self.assertEquals(thread.notified_dirs, [])
            self.assertEquals(thread.notified_fds,
                              [(thread, thread.assigned_fd)])
            thread.assigned_fd.close()

    def tearDown(self):
        self._temp_dir.cleanup()
        self.assertEquals(self._orig_fds, self.list_opened_fds())


class FSWatcherGenericTestCase(FSWatcherBaseTestCase, unittest.TestCase):
    watcher_class = fs_watcher.FSWatcherGeneric


if hasattr(select, 'kqueue'):
    class FSWatcherKQueueTestCase(FSWatcherBaseTestCase, unittest.TestCase):
        watcher_class = fs_watcher.FSWatcherKQueue

if pyinotify is not None:
    class FSWatcherINotifyTestCase(FSWatcherBaseTestCase, unittest.TestCase):
        watcher_class = fs_watcher.FSWatcherINotify
